package ru.yuzum.webchat.web.rest;

import ru.yuzum.webchat.EducationWebChatApp;
import ru.yuzum.webchat.domain.Lesson;
import ru.yuzum.webchat.domain.Room;
import ru.yuzum.webchat.domain.Band;
import ru.yuzum.webchat.repository.LessonRepository;
import ru.yuzum.webchat.service.LessonService;
import ru.yuzum.webchat.service.dto.LessonDTO;
import ru.yuzum.webchat.service.mapper.LessonMapper;
import ru.yuzum.webchat.service.dto.LessonCriteria;
import ru.yuzum.webchat.service.LessonQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ru.yuzum.webchat.domain.enumeration.DayOfWeek;
/**
 * Integration tests for the {@link LessonResource} REST controller.
 */
@SpringBootTest(classes = EducationWebChatApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class LessonResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final DayOfWeek DEFAULT_DAY_OF_WEEK = DayOfWeek.MONDAY;
    private static final DayOfWeek UPDATED_DAY_OF_WEEK = DayOfWeek.TUESDAY;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private LessonMapper lessonMapper;

    @Autowired
    private LessonService lessonService;

    @Autowired
    private LessonQueryService lessonQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLessonMockMvc;

    private Lesson lesson;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Lesson createEntity(EntityManager em) {
        Lesson lesson = new Lesson()
            .name(DEFAULT_NAME)
            .time(DEFAULT_TIME)
            .dayOfWeek(DEFAULT_DAY_OF_WEEK);
        return lesson;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Lesson createUpdatedEntity(EntityManager em) {
        Lesson lesson = new Lesson()
            .name(UPDATED_NAME)
            .time(UPDATED_TIME)
            .dayOfWeek(UPDATED_DAY_OF_WEEK);
        return lesson;
    }

    @BeforeEach
    public void initTest() {
        lesson = createEntity(em);
    }

    @Test
    @Transactional
    public void createLesson() throws Exception {
        int databaseSizeBeforeCreate = lessonRepository.findAll().size();

        // Create the Lesson
        LessonDTO lessonDTO = lessonMapper.toDto(lesson);
        restLessonMockMvc.perform(post("/api/lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lessonDTO)))
            .andExpect(status().isCreated());

        // Validate the Lesson in the database
        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeCreate + 1);
        Lesson testLesson = lessonList.get(lessonList.size() - 1);
        assertThat(testLesson.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testLesson.getTime()).isEqualTo(DEFAULT_TIME);
        assertThat(testLesson.getDayOfWeek()).isEqualTo(DEFAULT_DAY_OF_WEEK);
    }

    @Test
    @Transactional
    public void createLessonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = lessonRepository.findAll().size();

        // Create the Lesson with an existing ID
        lesson.setId(1L);
        LessonDTO lessonDTO = lessonMapper.toDto(lesson);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLessonMockMvc.perform(post("/api/lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lessonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Lesson in the database
        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = lessonRepository.findAll().size();
        // set the field null
        lesson.setName(null);

        // Create the Lesson, which fails.
        LessonDTO lessonDTO = lessonMapper.toDto(lesson);

        restLessonMockMvc.perform(post("/api/lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lessonDTO)))
            .andExpect(status().isBadRequest());

        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = lessonRepository.findAll().size();
        // set the field null
        lesson.setTime(null);

        // Create the Lesson, which fails.
        LessonDTO lessonDTO = lessonMapper.toDto(lesson);

        restLessonMockMvc.perform(post("/api/lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lessonDTO)))
            .andExpect(status().isBadRequest());

        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLessons() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList
        restLessonMockMvc.perform(get("/api/lessons?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lesson.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].time").value(hasItem(DEFAULT_TIME.toString())))
            .andExpect(jsonPath("$.[*].dayOfWeek").value(hasItem(DEFAULT_DAY_OF_WEEK.toString())));
    }
    
    @Test
    @Transactional
    public void getLesson() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get the lesson
        restLessonMockMvc.perform(get("/api/lessons/{id}", lesson.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(lesson.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.time").value(DEFAULT_TIME.toString()))
            .andExpect(jsonPath("$.dayOfWeek").value(DEFAULT_DAY_OF_WEEK.toString()));
    }


    @Test
    @Transactional
    public void getLessonsByIdFiltering() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        Long id = lesson.getId();

        defaultLessonShouldBeFound("id.equals=" + id);
        defaultLessonShouldNotBeFound("id.notEquals=" + id);

        defaultLessonShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLessonShouldNotBeFound("id.greaterThan=" + id);

        defaultLessonShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLessonShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLessonsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where name equals to DEFAULT_NAME
        defaultLessonShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the lessonList where name equals to UPDATED_NAME
        defaultLessonShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllLessonsByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where name not equals to DEFAULT_NAME
        defaultLessonShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the lessonList where name not equals to UPDATED_NAME
        defaultLessonShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllLessonsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where name in DEFAULT_NAME or UPDATED_NAME
        defaultLessonShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the lessonList where name equals to UPDATED_NAME
        defaultLessonShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllLessonsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where name is not null
        defaultLessonShouldBeFound("name.specified=true");

        // Get all the lessonList where name is null
        defaultLessonShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllLessonsByNameContainsSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where name contains DEFAULT_NAME
        defaultLessonShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the lessonList where name contains UPDATED_NAME
        defaultLessonShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllLessonsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where name does not contain DEFAULT_NAME
        defaultLessonShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the lessonList where name does not contain UPDATED_NAME
        defaultLessonShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllLessonsByTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where time equals to DEFAULT_TIME
        defaultLessonShouldBeFound("time.equals=" + DEFAULT_TIME);

        // Get all the lessonList where time equals to UPDATED_TIME
        defaultLessonShouldNotBeFound("time.equals=" + UPDATED_TIME);
    }

    @Test
    @Transactional
    public void getAllLessonsByTimeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where time not equals to DEFAULT_TIME
        defaultLessonShouldNotBeFound("time.notEquals=" + DEFAULT_TIME);

        // Get all the lessonList where time not equals to UPDATED_TIME
        defaultLessonShouldBeFound("time.notEquals=" + UPDATED_TIME);
    }

    @Test
    @Transactional
    public void getAllLessonsByTimeIsInShouldWork() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where time in DEFAULT_TIME or UPDATED_TIME
        defaultLessonShouldBeFound("time.in=" + DEFAULT_TIME + "," + UPDATED_TIME);

        // Get all the lessonList where time equals to UPDATED_TIME
        defaultLessonShouldNotBeFound("time.in=" + UPDATED_TIME);
    }

    @Test
    @Transactional
    public void getAllLessonsByTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where time is not null
        defaultLessonShouldBeFound("time.specified=true");

        // Get all the lessonList where time is null
        defaultLessonShouldNotBeFound("time.specified=false");
    }

    @Test
    @Transactional
    public void getAllLessonsByDayOfWeekIsEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where dayOfWeek equals to DEFAULT_DAY_OF_WEEK
        defaultLessonShouldBeFound("dayOfWeek.equals=" + DEFAULT_DAY_OF_WEEK);

        // Get all the lessonList where dayOfWeek equals to UPDATED_DAY_OF_WEEK
        defaultLessonShouldNotBeFound("dayOfWeek.equals=" + UPDATED_DAY_OF_WEEK);
    }

    @Test
    @Transactional
    public void getAllLessonsByDayOfWeekIsNotEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where dayOfWeek not equals to DEFAULT_DAY_OF_WEEK
        defaultLessonShouldNotBeFound("dayOfWeek.notEquals=" + DEFAULT_DAY_OF_WEEK);

        // Get all the lessonList where dayOfWeek not equals to UPDATED_DAY_OF_WEEK
        defaultLessonShouldBeFound("dayOfWeek.notEquals=" + UPDATED_DAY_OF_WEEK);
    }

    @Test
    @Transactional
    public void getAllLessonsByDayOfWeekIsInShouldWork() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where dayOfWeek in DEFAULT_DAY_OF_WEEK or UPDATED_DAY_OF_WEEK
        defaultLessonShouldBeFound("dayOfWeek.in=" + DEFAULT_DAY_OF_WEEK + "," + UPDATED_DAY_OF_WEEK);

        // Get all the lessonList where dayOfWeek equals to UPDATED_DAY_OF_WEEK
        defaultLessonShouldNotBeFound("dayOfWeek.in=" + UPDATED_DAY_OF_WEEK);
    }

    @Test
    @Transactional
    public void getAllLessonsByDayOfWeekIsNullOrNotNull() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        // Get all the lessonList where dayOfWeek is not null
        defaultLessonShouldBeFound("dayOfWeek.specified=true");

        // Get all the lessonList where dayOfWeek is null
        defaultLessonShouldNotBeFound("dayOfWeek.specified=false");
    }

    @Test
    @Transactional
    public void getAllLessonsByRoomIsEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);
        Room room = RoomResourceIT.createEntity(em);
        em.persist(room);
        em.flush();
        lesson.setRoom(room);
        lessonRepository.saveAndFlush(lesson);
        Long roomId = room.getId();

        // Get all the lessonList where room equals to roomId
        defaultLessonShouldBeFound("roomId.equals=" + roomId);

        // Get all the lessonList where room equals to roomId + 1
        defaultLessonShouldNotBeFound("roomId.equals=" + (roomId + 1));
    }


    @Test
    @Transactional
    public void getAllLessonsByBandIsEqualToSomething() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);
        Band band = BandResourceIT.createEntity(em);
        em.persist(band);
        em.flush();
        lesson.setBand(band);
        lessonRepository.saveAndFlush(lesson);
        Long bandId = band.getId();

        // Get all the lessonList where band equals to bandId
        defaultLessonShouldBeFound("bandId.equals=" + bandId);

        // Get all the lessonList where band equals to bandId + 1
        defaultLessonShouldNotBeFound("bandId.equals=" + (bandId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLessonShouldBeFound(String filter) throws Exception {
        restLessonMockMvc.perform(get("/api/lessons?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lesson.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].time").value(hasItem(DEFAULT_TIME.toString())))
            .andExpect(jsonPath("$.[*].dayOfWeek").value(hasItem(DEFAULT_DAY_OF_WEEK.toString())));

        // Check, that the count call also returns 1
        restLessonMockMvc.perform(get("/api/lessons/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLessonShouldNotBeFound(String filter) throws Exception {
        restLessonMockMvc.perform(get("/api/lessons?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLessonMockMvc.perform(get("/api/lessons/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingLesson() throws Exception {
        // Get the lesson
        restLessonMockMvc.perform(get("/api/lessons/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLesson() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        int databaseSizeBeforeUpdate = lessonRepository.findAll().size();

        // Update the lesson
        Lesson updatedLesson = lessonRepository.findById(lesson.getId()).get();
        // Disconnect from session so that the updates on updatedLesson are not directly saved in db
        em.detach(updatedLesson);
        updatedLesson
            .name(UPDATED_NAME)
            .time(UPDATED_TIME)
            .dayOfWeek(UPDATED_DAY_OF_WEEK);
        LessonDTO lessonDTO = lessonMapper.toDto(updatedLesson);

        restLessonMockMvc.perform(put("/api/lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lessonDTO)))
            .andExpect(status().isOk());

        // Validate the Lesson in the database
        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeUpdate);
        Lesson testLesson = lessonList.get(lessonList.size() - 1);
        assertThat(testLesson.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testLesson.getTime()).isEqualTo(UPDATED_TIME);
        assertThat(testLesson.getDayOfWeek()).isEqualTo(UPDATED_DAY_OF_WEEK);
    }

    @Test
    @Transactional
    public void updateNonExistingLesson() throws Exception {
        int databaseSizeBeforeUpdate = lessonRepository.findAll().size();

        // Create the Lesson
        LessonDTO lessonDTO = lessonMapper.toDto(lesson);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLessonMockMvc.perform(put("/api/lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lessonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Lesson in the database
        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLesson() throws Exception {
        // Initialize the database
        lessonRepository.saveAndFlush(lesson);

        int databaseSizeBeforeDelete = lessonRepository.findAll().size();

        // Delete the lesson
        restLessonMockMvc.perform(delete("/api/lessons/{id}", lesson.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Lesson> lessonList = lessonRepository.findAll();
        assertThat(lessonList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
