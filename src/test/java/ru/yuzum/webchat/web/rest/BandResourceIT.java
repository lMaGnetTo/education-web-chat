package ru.yuzum.webchat.web.rest;

import ru.yuzum.webchat.EducationWebChatApp;
import ru.yuzum.webchat.domain.Band;
import ru.yuzum.webchat.domain.Lesson;
import ru.yuzum.webchat.domain.Institution;
import ru.yuzum.webchat.domain.Profile;
import ru.yuzum.webchat.repository.BandRepository;
import ru.yuzum.webchat.service.BandService;
import ru.yuzum.webchat.service.dto.BandDTO;
import ru.yuzum.webchat.service.mapper.BandMapper;
import ru.yuzum.webchat.service.dto.BandCriteria;
import ru.yuzum.webchat.service.BandQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BandResource} REST controller.
 */
@SpringBootTest(classes = EducationWebChatApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class BandResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private BandRepository bandRepository;

    @Autowired
    private BandMapper bandMapper;

    @Autowired
    private BandService bandService;

    @Autowired
    private BandQueryService bandQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBandMockMvc;

    private Band band;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Band createEntity(EntityManager em) {
        Band band = new Band()
            .name(DEFAULT_NAME);
        return band;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Band createUpdatedEntity(EntityManager em) {
        Band band = new Band()
            .name(UPDATED_NAME);
        return band;
    }

    @BeforeEach
    public void initTest() {
        band = createEntity(em);
    }

    @Test
    @Transactional
    public void createBand() throws Exception {
        int databaseSizeBeforeCreate = bandRepository.findAll().size();

        // Create the Band
        BandDTO bandDTO = bandMapper.toDto(band);
        restBandMockMvc.perform(post("/api/bands")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bandDTO)))
            .andExpect(status().isCreated());

        // Validate the Band in the database
        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeCreate + 1);
        Band testBand = bandList.get(bandList.size() - 1);
        assertThat(testBand.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createBandWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bandRepository.findAll().size();

        // Create the Band with an existing ID
        band.setId(1L);
        BandDTO bandDTO = bandMapper.toDto(band);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBandMockMvc.perform(post("/api/bands")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bandDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Band in the database
        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = bandRepository.findAll().size();
        // set the field null
        band.setName(null);

        // Create the Band, which fails.
        BandDTO bandDTO = bandMapper.toDto(band);

        restBandMockMvc.perform(post("/api/bands")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bandDTO)))
            .andExpect(status().isBadRequest());

        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBands() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList
        restBandMockMvc.perform(get("/api/bands?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(band.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getBand() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get the band
        restBandMockMvc.perform(get("/api/bands/{id}", band.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(band.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }


    @Test
    @Transactional
    public void getBandsByIdFiltering() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        Long id = band.getId();

        defaultBandShouldBeFound("id.equals=" + id);
        defaultBandShouldNotBeFound("id.notEquals=" + id);

        defaultBandShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBandShouldNotBeFound("id.greaterThan=" + id);

        defaultBandShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBandShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllBandsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where name equals to DEFAULT_NAME
        defaultBandShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the bandList where name equals to UPDATED_NAME
        defaultBandShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllBandsByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where name not equals to DEFAULT_NAME
        defaultBandShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the bandList where name not equals to UPDATED_NAME
        defaultBandShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllBandsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where name in DEFAULT_NAME or UPDATED_NAME
        defaultBandShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the bandList where name equals to UPDATED_NAME
        defaultBandShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllBandsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where name is not null
        defaultBandShouldBeFound("name.specified=true");

        // Get all the bandList where name is null
        defaultBandShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllBandsByNameContainsSomething() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where name contains DEFAULT_NAME
        defaultBandShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the bandList where name contains UPDATED_NAME
        defaultBandShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllBandsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where name does not contain DEFAULT_NAME
        defaultBandShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the bandList where name does not contain UPDATED_NAME
        defaultBandShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllBandsByLessonIsEqualToSomething() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);
        Lesson lesson = LessonResourceIT.createEntity(em);
        em.persist(lesson);
        em.flush();
        band.addLesson(lesson);
        bandRepository.saveAndFlush(band);
        Long lessonId = lesson.getId();

        // Get all the bandList where lesson equals to lessonId
        defaultBandShouldBeFound("lessonId.equals=" + lessonId);

        // Get all the bandList where lesson equals to lessonId + 1
        defaultBandShouldNotBeFound("lessonId.equals=" + (lessonId + 1));
    }


    @Test
    @Transactional
    public void getAllBandsByInstitutionIsEqualToSomething() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);
        Institution institution = InstitutionResourceIT.createEntity(em);
        em.persist(institution);
        em.flush();
        band.setInstitution(institution);
        bandRepository.saveAndFlush(band);
        Long institutionId = institution.getId();

        // Get all the bandList where institution equals to institutionId
        defaultBandShouldBeFound("institutionId.equals=" + institutionId);

        // Get all the bandList where institution equals to institutionId + 1
        defaultBandShouldNotBeFound("institutionId.equals=" + (institutionId + 1));
    }


    @Test
    @Transactional
    public void getAllBandsByProfilesIsEqualToSomething() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);
        Profile profiles = ProfileResourceIT.createEntity(em);
        em.persist(profiles);
        em.flush();
        band.addProfiles(profiles);
        bandRepository.saveAndFlush(band);
        Long profilesId = profiles.getId();

        // Get all the bandList where profiles equals to profilesId
        defaultBandShouldBeFound("profilesId.equals=" + profilesId);

        // Get all the bandList where profiles equals to profilesId + 1
        defaultBandShouldNotBeFound("profilesId.equals=" + (profilesId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBandShouldBeFound(String filter) throws Exception {
        restBandMockMvc.perform(get("/api/bands?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(band.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restBandMockMvc.perform(get("/api/bands/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBandShouldNotBeFound(String filter) throws Exception {
        restBandMockMvc.perform(get("/api/bands?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBandMockMvc.perform(get("/api/bands/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingBand() throws Exception {
        // Get the band
        restBandMockMvc.perform(get("/api/bands/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBand() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        int databaseSizeBeforeUpdate = bandRepository.findAll().size();

        // Update the band
        Band updatedBand = bandRepository.findById(band.getId()).get();
        // Disconnect from session so that the updates on updatedBand are not directly saved in db
        em.detach(updatedBand);
        updatedBand
            .name(UPDATED_NAME);
        BandDTO bandDTO = bandMapper.toDto(updatedBand);

        restBandMockMvc.perform(put("/api/bands")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bandDTO)))
            .andExpect(status().isOk());

        // Validate the Band in the database
        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeUpdate);
        Band testBand = bandList.get(bandList.size() - 1);
        assertThat(testBand.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingBand() throws Exception {
        int databaseSizeBeforeUpdate = bandRepository.findAll().size();

        // Create the Band
        BandDTO bandDTO = bandMapper.toDto(band);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBandMockMvc.perform(put("/api/bands")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bandDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Band in the database
        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBand() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        int databaseSizeBeforeDelete = bandRepository.findAll().size();

        // Delete the band
        restBandMockMvc.perform(delete("/api/bands/{id}", band.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
