import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './institution.reducer';
import { IInstitution } from 'app/shared/model/institution.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IInstitutionProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Institution = (props: IInstitutionProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { institutionList, match, loading } = props;
  return (
    <div>
      <h4 id="institution-heading">
        <Translate contentKey="educationWebChatApp.institution.home.title">Institutions</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="educationWebChatApp.institution.home.createLabel">Create new Institution</Translate>
        </Link>
      </h4>
        {institutionList && institutionList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="educationWebChatApp.institution.name">Name</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {institutionList.map((institution, i) => (
                <tr key={`entity-${i}`}>
                  <td>{institution.name}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${institution.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${institution.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${institution.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="educationWebChatApp.institution.home.notFound">No Institutions found</Translate>
            </div>
          )
        )}
    </div>
  );
};

const mapStateToProps = ({ institution }: IRootState) => ({
  institutionList: institution.entities,
  loading: institution.loading
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Institution);
