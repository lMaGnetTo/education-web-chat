import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Institution from './institution';
import InstitutionDetail from './institution-detail';
import InstitutionUpdate from './institution-update';
import InstitutionDeleteDialog from './institution-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={InstitutionDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={InstitutionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={InstitutionUpdate} />
      <ErrorBoundaryRoute path={`${match.path}/:id`} component={InstitutionDetail} />
      <ErrorBoundaryRoute path={match.url} component={Institution} />
    </Switch>
  </>
);

export default Routes;
