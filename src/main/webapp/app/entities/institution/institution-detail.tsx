import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Switch, Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './institution.reducer';
import { IInstitution } from 'app/shared/model/institution.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

// import { EntitiesList } from 'app/shared/components/entities-list'
import Band from 'app/entities/band';

export interface IInstitutionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const InstitutionDetail = (props: IInstitutionDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);


  // eslint-disable-next-line no-console
  console.log('InstitutionDetail Routes match prop', props.match)

  const { institutionEntity, match } = props;
  return (
    <>
      {/* <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="educationWebChatApp.institution.detail.title">Institution</Translate> [<b>{institutionEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">
                <Translate contentKey="educationWebChatApp.institution.name">Name</Translate>
              </span>
            </dt>
            <dd>{institutionEntity.name}</dd>
          </dl>
          <Button tag={Link} to="/institution" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/institution/${institutionEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row> */}

      <div>
        <dl>
          <dt>
            <span><Translate contentKey="educationWebChatApp.institution.detail.title">Institution</Translate>: </span>
          </dt>
          <dd><span>{institutionEntity.name ? institutionEntity.name : ''}</span></dd>
        </dl>
      </div>

      <Band
        entityUrl="/band"
        entityParam="/:bandId"
        match={match}
      />
      {/* <Switch>
        <ErrorBoundaryRoute path={match.url} component={Band} />
      </Switch> */}
    </>
  );
};

const mapStateToProps = ({ institution }: IRootState) => ({
  institutionEntity: institution.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(InstitutionDetail);
