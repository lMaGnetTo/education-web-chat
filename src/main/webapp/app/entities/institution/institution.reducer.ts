import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IInstitution, defaultValue } from 'app/shared/model/institution.model';

export const ACTION_TYPES = {
  FETCH_INSTITUTION_LIST: 'institution/FETCH_INSTITUTION_LIST',
  FETCH_INSTITUTION: 'institution/FETCH_INSTITUTION',
  CREATE_INSTITUTION: 'institution/CREATE_INSTITUTION',
  UPDATE_INSTITUTION: 'institution/UPDATE_INSTITUTION',
  DELETE_INSTITUTION: 'institution/DELETE_INSTITUTION',
  RESET: 'institution/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IInstitution>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type InstitutionState = Readonly<typeof initialState>;

// Reducer

export default (state: InstitutionState = initialState, action): InstitutionState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_INSTITUTION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_INSTITUTION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_INSTITUTION):
    case REQUEST(ACTION_TYPES.UPDATE_INSTITUTION):
    case REQUEST(ACTION_TYPES.DELETE_INSTITUTION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_INSTITUTION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_INSTITUTION):
    case FAILURE(ACTION_TYPES.CREATE_INSTITUTION):
    case FAILURE(ACTION_TYPES.UPDATE_INSTITUTION):
    case FAILURE(ACTION_TYPES.DELETE_INSTITUTION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_INSTITUTION_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_INSTITUTION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_INSTITUTION):
    case SUCCESS(ACTION_TYPES.UPDATE_INSTITUTION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_INSTITUTION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/institutions';

// Actions

export const getEntities: ICrudGetAllAction<IInstitution> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_INSTITUTION_LIST,
  payload: axios.get<IInstitution>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IInstitution> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_INSTITUTION,
    payload: axios.get<IInstitution>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IInstitution> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_INSTITUTION,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IInstitution> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_INSTITUTION,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IInstitution> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_INSTITUTION,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
