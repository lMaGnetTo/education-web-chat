import axios from 'axios';
import { ICrudGetAction, ICrudPutAction, ICrudDeleteAction, IPayload } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IBand, defaultValue } from 'app/shared/model/band.model';

export const ACTION_TYPES = {
  FETCH_BAND_LIST: 'band/FETCH_BAND_LIST',
  FETCH_BAND: 'band/FETCH_BAND',
  CREATE_BAND: 'band/CREATE_BAND',
  UPDATE_BAND: 'band/UPDATE_BAND',
  DELETE_BAND: 'band/DELETE_BAND',
  RESET: 'band/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IBand>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type BandState = Readonly<typeof initialState>;

// Reducer

export default (state: BandState = initialState, action): BandState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_BAND_LIST):
    case REQUEST(ACTION_TYPES.FETCH_BAND):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_BAND):
    case REQUEST(ACTION_TYPES.UPDATE_BAND):
    case REQUEST(ACTION_TYPES.DELETE_BAND):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_BAND_LIST):
    case FAILURE(ACTION_TYPES.FETCH_BAND):
    case FAILURE(ACTION_TYPES.CREATE_BAND):
    case FAILURE(ACTION_TYPES.UPDATE_BAND):
    case FAILURE(ACTION_TYPES.DELETE_BAND):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_BAND_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_BAND):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_BAND):
    case SUCCESS(ACTION_TYPES.UPDATE_BAND):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_BAND):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/bands';

// Actions
declare type ICrudGetAllAction<T> = (
  id?: string,
  page?: number,
  size?: number,
  sort?: string
) => IPayload<T> | ((dispatch: any) => IPayload<T>);

export const getEntities: ICrudGetAllAction<IBand> = (id, page, size, sort) => ({
  type: ACTION_TYPES.FETCH_BAND_LIST,
  payload: axios.get<IBand>(`${apiUrl}?cacheBuster=${new Date().getTime()}&institutionId.equals=${id}`)
});

export const getEntity: ICrudGetAction<IBand> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_BAND,
    payload: axios.get<IBand>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IBand> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_BAND,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IBand> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_BAND,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IBand> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_BAND,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
