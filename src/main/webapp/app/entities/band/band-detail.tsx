import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps, Switch} from 'react-router-dom';
import {Button, Row, Col} from 'reactstrap';
import {Translate, ICrudGetAction, log} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {IRootState} from 'app/shared/reducers';
import {getEntity} from './band.reducer';
import {IBand} from 'app/shared/model/band.model';
import {APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT} from 'app/config/constants';

// Lesson imports
import Lesson from 'app/entities/lesson'
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

export interface IBandDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ bandId: string }> {
}

export const BandDetail = (props: IBandDetailProps) => {

  // eslint-disable-next-line no-console
  console.log('Banddetails Routes match', props.match);

  useEffect(() => {
    props.getEntity(props.match.params.bandId);
  }, []);

  const {bandEntity, match} = props;
  // const backUrl = match.url.slice(0, match.url.length - 7);
  const backUrl = match.url.split('/').splice(0, 3).join('/');

  return (
    <>
      <Row>
        <Col md="8">
          <div>
            <dl>
              <dt>
                <span><Translate contentKey="educationWebChatApp.band.institution">Institution</Translate>: </span>
              </dt>
              <dd><span>{bandEntity.institutionName ? bandEntity.institutionName : ''}</span></dd>
              <dt>
                <span><Translate contentKey="educationWebChatApp.band.detail.title">Band</Translate>: </span>
              </dt>
              <dd><span>{bandEntity.name ? bandEntity.name : ''}</span></dd>
            </dl>
          </div>
        </Col>
        <Col md="4">
          <Button tag={Link} to={`${backUrl}/edit`} replace color="primary"
                  className="float-right">
            <FontAwesomeIcon icon="pencil-alt"/>{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
          <Button tag={Link} to={backUrl} replace color="info" className="float-right mr-2">
            <FontAwesomeIcon icon="arrow-left"/>{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
        </Col>
      </Row>

      <br/>

      <Lesson match={match} />
      {/* <Switch>
        <ErrorBoundaryRoute path={match.url} component={Lesson} />
      </Switch> */}
    </>
  );
};

const mapStateToProps = ({band}: IRootState) => ({
  bandEntity: band.entity
});

const mapDispatchToProps = {getEntity};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(BandDetail);
