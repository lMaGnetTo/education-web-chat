import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Band from './band';
import BandDetail from './band-detail';
import BandUpdate from './band-update';
import BandDeleteDialog from './band-delete-dialog';

const Routes = ({ match, entityUrl, entityParam }) => {

  // eslint-disable-next-line no-console
  console.log('Band Routes match', match);

  return (
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:bandId/delete`} component={BandDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={BandUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:bandId/edit`} component={BandUpdate} />
      <ErrorBoundaryRoute path={`${match.path}/band/:bandId`} component={BandDetail} />
      <ErrorBoundaryRoute path={match.url} component={Band} />

      {/* <ErrorBoundaryRoute exact path={`${match.path}${entityUrl}${entityParam}/delete`} component={BandDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.path}${entityUrl}/new`} component={BandUpdate} />
      <ErrorBoundaryRoute exact path={`${match.path}${entityUrl}${entityParam}/edit`} component={BandUpdate} /> */}
      {/* <ErrorBoundaryRoute exact path={`${match.path}/band/:bandId`} component={BandDetail} />
      <ErrorBoundaryRoute exact path={match.path} component={Band} /> */}
    </Switch>
)};

export default Routes;
