import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Translate, ICrudGetAction, ICrudDeleteAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IBand } from 'app/shared/model/band.model';
import { IRootState } from 'app/shared/reducers';
import { getEntity, deleteEntity } from './band.reducer';

export interface IBandDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const BandDeleteDialog = (props: IBandDeleteDialogProps) => {
  const backUrl = props.match.url.split('/').splice(0, 3).join('/');
  // eslint-disable-next-line no-console
  console.log(backUrl);
  
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);


  const handleClose = () => {
    props.history.push(backUrl);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const confirmDelete = () => {
    props.deleteEntity(props.bandEntity.id);
  };

  const { bandEntity } = props;
  return (
    <Modal isOpen toggle={handleClose}>
      <ModalHeader toggle={handleClose}>
        <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
      </ModalHeader>
      <ModalBody id="educationWebChatApp.band.delete.question">
        <Translate contentKey="educationWebChatApp.band.delete.question" interpolate={{ id: bandEntity.id }}>
          Are you sure you want to delete this Band?
        </Translate>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={handleClose}>
          <FontAwesomeIcon icon="ban" />
          &nbsp;
          <Translate contentKey="entity.action.cancel">Cancel</Translate>
        </Button>
        <Button id="jhi-confirm-delete-band" color="danger" onClick={confirmDelete}>
          <FontAwesomeIcon icon="trash" />
          &nbsp;
          <Translate contentKey="entity.action.delete">Delete</Translate>
        </Button>
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = ({ band }: IRootState) => ({
  bandEntity: band.entity,
  updateSuccess: band.updateSuccess
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(BandDeleteDialog);
