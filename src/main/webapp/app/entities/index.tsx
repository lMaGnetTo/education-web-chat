import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Lesson from './lesson';
import Room from './room';
import Band from './band';
import Profile from './profile';
import Institution from './institution';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => {
  // eslint-disable-next-line no-console
  // console.log('Routes match prop', match)

  return (
    <div>
      <Switch>
        {/* prettier-ignore */}
        {/* <ErrorBoundaryRoute path={`/institution/:id/band/:bandId/lesson`} component={Lesson} /> */}
        <ErrorBoundaryRoute path={`${match.url}room`} component={Room} />
        {/* <ErrorBoundaryRoute path={`/institution/:id/band`} component={Band} /> */}
        <ErrorBoundaryRoute path={`${match.url}profile`} component={Profile} />
        <ErrorBoundaryRoute path={`${match.path}institution`} component={Institution} />
        {/* jhipster-needle-add-route-path - JHipster will add routes here */}
      </Switch>
    </div>
  )
};

export default Routes;
