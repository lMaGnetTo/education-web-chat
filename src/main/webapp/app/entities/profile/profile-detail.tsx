import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './profile.reducer';
import { IProfile } from 'app/shared/model/profile.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProfileDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProfileDetail = (props: IProfileDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  // eslint-disable-next-line no-console
  console.log('Profile details Routes match', props.match);

  const { profileEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="educationWebChatApp.profile.detail.title">Profile</Translate> [<b>{profileEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="fullName">
              <Translate contentKey="educationWebChatApp.profile.fullName">Full Name</Translate>
            </span>
          </dt>
          <dd>{profileEntity.fullName}</dd>
          <dt>
            <Translate contentKey="educationWebChatApp.profile.user">User</Translate>
          </dt>
          <dd>{profileEntity.userId ? profileEntity.userId : ''}</dd>
          <dt>
            <Translate contentKey="educationWebChatApp.profile.bands">Bands</Translate>
          </dt>
          <dd>
            {profileEntity.bands
              ? profileEntity.bands.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {i === profileEntity.bands.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
        </dl>
        <Button tag={Link} to="/profile" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/profile/${profileEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ profile }: IRootState) => ({
  profileEntity: profile.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProfileDetail);
