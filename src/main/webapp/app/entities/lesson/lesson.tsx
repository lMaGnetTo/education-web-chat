import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {RouteComponentProps} from 'react-router-dom';
import {Translate} from 'react-jhipster';

import {IRootState} from 'app/shared/reducers';
import {getEntities} from './lesson.reducer';
import {LessonItem} from "app/entities/lesson/lesson-item";

export interface ILessonProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string, bandId: string }> {
}

export const Lesson = (props: ILessonProps) => {
  useEffect(() => {
    props.getEntities(props.match.params.bandId);
  }, []);

  const { lessonList, match, loading } = props;

  const weekDays = {
    "MONDAY": [],
    "TUESDAY": [],
    "WEDNESDAY": [],
    "THURSDAY": [],
    "FRIDAY": [],
    "SATURDAY": [],
    "SUNDAY": []
  };


  // eslint-disable-next-line no-console
  console.log('Lessonlist match', match);

  lessonList.forEach(day => {
    weekDays[day.dayOfWeek].push(day)
  });

  return (
    <div>
      <h4 id="lesson-heading" className="text-center">
        <Translate contentKey="educationWebChatApp.lesson.timetable">Timetable</Translate>
      </h4>
        {lessonList && lessonList.length > 0 ? (
          <>
            {Object.keys(weekDays).map((day, i) => (
              <>
                {(weekDays[day] && weekDays[day].length > 0) ? (
                  <LessonItem dayOfWeek={day} lessons={weekDays[day]} match={match}/>
                ) : (
                  <></>
                )}
              </>
            ))}
          </>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="educationWebChatApp.lesson.home.notFound">No Lessons found</Translate>
            </div>
          )
        )}
    </div>
  );
};

const mapStateToProps = ({lesson}: IRootState) => ({
  lessonList: lesson.entities,
  loading: lesson.loading
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Lesson);
