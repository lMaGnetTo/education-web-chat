import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './lesson.reducer';
import { ILesson } from 'app/shared/model/lesson.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILessonDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string, lessonId: string }> {}

export const LessonDetail = (props: ILessonDetailProps) => {
  // eslint-disable-next-line no-console
  console.log('LessonDetails params', props);

  useEffect(() => {
    props.getEntity(props.match.params.lessonId);
  }, []);

  const { lessonEntity } = props;

  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="educationWebChatApp.lesson.detail.title">Lesson</Translate> [<b>{lessonEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">
              <Translate contentKey="educationWebChatApp.lesson.name">Name</Translate>
            </span>
          </dt>
          <dd>{lessonEntity.name}</dd>
          <dt>
            <span id="time">
              <Translate contentKey="educationWebChatApp.lesson.time">Time</Translate>
            </span>
          </dt>
          <dd>
            <TextFormat value={lessonEntity.time} type="date" format={APP_DATE_FORMAT} />
          </dd>
          <dt>
            <span id="dayOfWeek">
              <Translate contentKey="educationWebChatApp.lesson.dayOfWeek">Day Of Week</Translate>
            </span>
          </dt>
          <dd>{lessonEntity.dayOfWeek}</dd>
          <dt>
            <Translate contentKey="educationWebChatApp.lesson.room">Room</Translate>
          </dt>
          <dd>{lessonEntity.roomId ? lessonEntity.roomId : ''}</dd>
          <dt>
            <Translate contentKey="educationWebChatApp.lesson.band">Band</Translate>
          </dt>
          <dd>{lessonEntity.bandId ? lessonEntity.bandId : ''}</dd>
        </dl>
        <Button tag={Link} to="/lesson" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/lesson/${lessonEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ lesson }: IRootState) => ({
  lessonEntity: lesson.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LessonDetail);
// export default LessonDetail;
