import React, {useEffect} from 'react';
import {Link} from 'react-router-dom';
import {Button, Table} from 'reactstrap';
import {TextFormat, Translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {LESSON_TIME_FORMAT} from 'app/config/constants';

export const LessonItem = ({dayOfWeek, lessons, match}) => {

  useEffect(() => {
  }, []);

  const dayOfWeekTransaction = "educationWebChatApp.DayOfWeek." + dayOfWeek;

  return (
    <div className="shadow p-3 mb-5 bg-white rounded mb-3 ml-3 mr-3">
      <h2 id="profile-heading">
        <span className="text-danger">
          <Translate contentKey={dayOfWeekTransaction}>{dayOfWeek}</Translate>
        </span>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus"/>
          &nbsp;
          <Translate contentKey="educationWebChatApp.lesson.home.createLabel">Name</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        <Table responsive className="table-striped">
          <thead>
          <tr>
            <th>
              <Translate contentKey="educationWebChatApp.lesson.name">Name</Translate>
            </th>
            <th>
              <Translate contentKey="educationWebChatApp.lesson.time">Time</Translate>
            </th>
          </tr>
          </thead>
          <tbody>
          {lessons.map((lesson, i) => (
            <tr key={`entity-${i}`}>
              <td>{lesson.name}</td>
              <td>
                <TextFormat type="date" value={lesson.time} format={LESSON_TIME_FORMAT}/>
              </td>
              <td className="text-right">
                <div className="btn-group flex-btn-group-container">
                  <Button tag={Link} to={`${match.url}/lesson/${lesson.id}`} color="info" size="sm">
                    <FontAwesomeIcon icon="eye"/>{' '}
                    <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                  </Button>
                  <Button tag={Link} to={`${match.url}/lesson/${lesson.id}/edit`} color="primary" size="sm">
                    <FontAwesomeIcon icon="pencil-alt"/>{' '}
                    <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                  </Button>
                  <Button tag={Link} to={`${match.url}/${lesson.id}/delete`} color="danger" size="sm">
                    <FontAwesomeIcon icon="trash"/>{' '}
                    <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                  </Button>
                </div>

                <Link to={`/webchat/?room=${lesson.roomIdentifier}`} target="_blank">
                  <Button color="warning" size="sm" className="ml-2">
                    <FontAwesomeIcon icon="user"/>{' '}
                    <span className="d-none d-md-inline">
                          <span>Join</span>
                        </span>
                  </Button>
                </Link>
              </td>
            </tr>
          ))}
          </tbody>
        </Table>
      </div>
    </div>
  )
};


export default LessonItem;
