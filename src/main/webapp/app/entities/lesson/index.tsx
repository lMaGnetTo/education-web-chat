import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Lesson from './lesson';
import LessonDetail from './lesson-detail';
import LessonUpdate from './lesson-update';
import LessonDeleteDialog from './lesson-delete-dialog';

const Routes = ({ match }) => {

  // eslint-disable-next-line no-console
  console.log('Lesson Routes match', match);

  return (
    <>
      <Switch>
        <ErrorBoundaryRoute exact path={`${match.url}/lesson/:lessonId/delete`} component={LessonDeleteDialog} />
        <ErrorBoundaryRoute exact path={`${match.url}/lesson/new`} component={LessonUpdate} />
        <ErrorBoundaryRoute path={`${match.path}/lesson/:lessonId/edit`} component={LessonUpdate} />
        <ErrorBoundaryRoute path={`${match.path}/lesson/:lessonId`} component={LessonDetail} />
        <ErrorBoundaryRoute path={match.path} component={Lesson} />

        {/* <ErrorBoundaryRoute exact path={`${match.path}/:id/delete`} component={LessonDeleteDialog} />
        <ErrorBoundaryRoute exact path={`${match.path}/new`} component={LessonUpdate} />
        <ErrorBoundaryRoute exact path={`${match.path}/:id/edit`} component={LessonUpdate} /> */}
        {/* <ErrorBoundaryRoute exact path={`${match.path}/lesson`} component={LessonDetail} />
        <ErrorBoundaryRoute path={match.path} component={Lesson} /> */}
      </Switch>
    </>
)
};

export default Routes;
