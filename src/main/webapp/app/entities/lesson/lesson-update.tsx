import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IRoom } from 'app/shared/model/room.model';
import { getEntities as getRooms } from 'app/entities/room/room.reducer';
import { IBand } from 'app/shared/model/band.model';
import { getEntities as getBands } from 'app/entities/band/band.reducer';
import { getEntity, updateEntity, createEntity, reset } from './lesson.reducer';
import { ILesson } from 'app/shared/model/lesson.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ILessonUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LessonUpdate = (props: ILessonUpdateProps) => {
  const [roomId, setRoomId] = useState('0');
  const [bandId, setBandId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { lessonEntity, rooms, bands, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/lesson');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getRooms();
    props.getBands();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.time = convertDateTimeToServer(values.time);

    if (errors.length === 0) {
      const entity = {
        ...lessonEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="educationWebChatApp.lesson.home.createOrEditLabel">
            <Translate contentKey="educationWebChatApp.lesson.home.createOrEditLabel">Create or edit a Lesson</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : lessonEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="lesson-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="lesson-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="lesson-name">
                  <Translate contentKey="educationWebChatApp.lesson.name">Name</Translate>
                </Label>
                <AvField
                  id="lesson-name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="timeLabel" for="lesson-time">
                  <Translate contentKey="educationWebChatApp.lesson.time">Time</Translate>
                </Label>
                <AvInput
                  id="lesson-time"
                  type="datetime-local"
                  className="form-control"
                  name="time"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.lessonEntity.time)}
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="dayOfWeekLabel" for="lesson-dayOfWeek">
                  <Translate contentKey="educationWebChatApp.lesson.dayOfWeek">Day Of Week</Translate>
                </Label>
                <AvInput
                  id="lesson-dayOfWeek"
                  type="select"
                  className="form-control"
                  name="dayOfWeek"
                  value={(!isNew && lessonEntity.dayOfWeek) || 'MONDAY'}
                >
                  <option value="MONDAY">{translate('educationWebChatApp.DayOfWeek.MONDAY')}</option>
                  <option value="TUESDAY">{translate('educationWebChatApp.DayOfWeek.TUESDAY')}</option>
                  <option value="WEDNESDAY">{translate('educationWebChatApp.DayOfWeek.WEDNESDAY')}</option>
                  <option value="THURSDAY">{translate('educationWebChatApp.DayOfWeek.THURSDAY')}</option>
                  <option value="FRIDAY">{translate('educationWebChatApp.DayOfWeek.FRIDAY')}</option>
                  <option value="SATURDAY">{translate('educationWebChatApp.DayOfWeek.SATURDAY')}</option>
                  <option value="SUNDAY">{translate('educationWebChatApp.DayOfWeek.SUNDAY')}</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="lesson-room">
                  <Translate contentKey="educationWebChatApp.lesson.room">Room</Translate>
                </Label>
                <AvInput id="lesson-room" type="select" className="form-control" name="roomId">
                  <option value="" key="0" />
                  {rooms
                    ? rooms.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="lesson-band">
                  <Translate contentKey="educationWebChatApp.lesson.band">Band</Translate>
                </Label>
                <AvInput id="lesson-band" type="select" className="form-control" name="bandId">
                  <option value="" key="0" />
                  {bands
                    ? bands.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/lesson" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  rooms: storeState.room.entities,
  bands: storeState.band.entities,
  lessonEntity: storeState.lesson.entity,
  loading: storeState.lesson.loading,
  updating: storeState.lesson.updating,
  updateSuccess: storeState.lesson.updateSuccess
});

const mapDispatchToProps = {
  getRooms,
  getBands,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LessonUpdate);
