import { IBand } from 'app/shared/model/band.model';

export interface IInstitution {
  id?: number;
  name?: string;
  bands?: IBand[];
}

export const defaultValue: Readonly<IInstitution> = {};
