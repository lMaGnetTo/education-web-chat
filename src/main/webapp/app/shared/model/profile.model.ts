import { IBand } from 'app/shared/model/band.model';

export interface IProfile {
  id?: number;
  fullName?: string;
  userId?: number;
  bands?: IBand[];
}

export const defaultValue: Readonly<IProfile> = {};
