import { ILesson } from 'app/shared/model/lesson.model';
import { IProfile } from 'app/shared/model/profile.model';

export interface IBand {
  id?: number;
  name?: string;
  lessons?: ILesson[];
  institutionId?: number;
  institutionName?: string;
  profiles?: IProfile[];
}

export const defaultValue: Readonly<IBand> = {};
