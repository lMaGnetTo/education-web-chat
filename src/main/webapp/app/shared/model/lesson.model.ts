import { Moment } from 'moment';
import { DayOfWeek } from 'app/shared/model/enumerations/day-of-week.model';

export interface ILesson {
  id?: number;
  name?: string;
  time?: Moment;
  dayOfWeek?: DayOfWeek;
  roomId?: number;
  roomIdentifier?: string;
  bandId?: number;
}

export const defaultValue: Readonly<ILesson> = {};
