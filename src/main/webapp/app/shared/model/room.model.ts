export interface IRoom {
  id?: number;
  identifier?: string;
  lessonId?: number;
}

export const defaultValue: Readonly<IRoom> = {};
