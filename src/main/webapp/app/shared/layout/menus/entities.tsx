import React, { SFC } from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { DropdownItem, NavItem, NavLink, Nav } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Translate, translate } from 'react-jhipster';
import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from './menu-components';

interface IEntitiesMenuItemProps {
  to: string;
}

const EntitiesMenuItem: SFC<IEntitiesMenuItemProps> = ({ to }) => (
  <NavItem>
    <NavLink tag={Link} to={`/${to}`}>
      {/* <FontAwesomeIcon icon={icon} fixedWidth /> */}
      <Translate contentKey={`global.menu.entities.${to}`} />
    </NavLink>
  </NavItem>
);

export const EntitiesMenu = props => (
  <NavItem className="mr-auto">
    <Nav className="p-0">
      <EntitiesMenuItem to="institution" />
      <EntitiesMenuItem to="band" />
      <EntitiesMenuItem to="lesson" />
      <EntitiesMenuItem to="profile" />
    </Nav>
  </NavItem>
);

// export const EntitiesMenu = props => (
//   <NavDropdown
//     icon="th-list"
//     name={translate('global.menu.entities.main')}
//     id="entity-menu"
//     style={{ maxHeight: '80vh', overflow: 'auto' }}
//   >
//     <MenuItem icon="asterisk" to="/lesson">
//       <Translate contentKey="global.menu.entities.lesson" />
//     </MenuItem>
//     <MenuItem icon="asterisk" to="/room">
//       <Translate contentKey="global.menu.entities.room" />
//     </MenuItem>
//     <MenuItem icon="asterisk" to="/band">
//       <Translate contentKey="global.menu.entities.band" />
//     </MenuItem>
//     <MenuItem icon="asterisk" to="/profile">
//       <Translate contentKey="global.menu.entities.profile" />
//     </MenuItem>
//     <MenuItem icon="asterisk" to="/institution">
//       <Translate contentKey="global.menu.entities.institution" />
//     </MenuItem>
//     {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
//   </NavDropdown>
// );
