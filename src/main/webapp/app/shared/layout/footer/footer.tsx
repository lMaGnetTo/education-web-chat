import './footer.scss';

import React from 'react';
import {Translate} from 'react-jhipster';
import {Col, Row, Container} from 'reactstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const Footer = props => (
  <div className="footer page-content pt-2">
    <Row className="p-3">
      <Col md="8">
        <span className="h5">Education Web Chat</span> <br/>
        <span>Provide the best experience on holding any educational activity.</span>
      </Col>
      <Col md="4">
        Developer contacts: <br/>
        R.I. <a href="mailto:magnusdawhore@gmail.com">magnusdawhore@gmail.com</a> <br/>
        R.B. <a href="mailto:rinat94ua@gmail.com">rinat94ua@gmail.com</a>
      </Col>
    </Row>
    <h6 className="text-center">Powered by <a href="//yuzum.ru">YUZUM</a> (2018-2020)</h6>
  </div>
);

export default Footer;
