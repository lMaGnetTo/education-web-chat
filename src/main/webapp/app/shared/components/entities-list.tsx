import React, { useEffect, SFC } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Table } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';

import Band from 'app/entities/band';

// export interface IInstitutionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

interface ParentEntity {
  id: number;
  name: string;
}

type EntitiesList = ParentEntity

interface IEntitiesListProps extends RouteComponentProps {
  parentEntity: ParentEntity;
  entitiesList: EntitiesList[];
  loading: boolean;
}

// export const EntitiesList: SFC<IEntitiesListProps> = ({ parentEntity, entitiesList, match, loading }) => {
export const EntitiesList = (props) => {
  // eslint-disable-next-line no-console
  console.log(props)

  const { parentEntityName, parentEntity, match } = props

  return (
    <>
      <Row>
        <Col md="12">
          <h2>
            <Translate contentKey={`educationWebChatApp.${parentEntityName}.detail.title`}>{parentEntityName}</Translate> [<b>{parentEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">
                <Translate contentKey={`educationWebChatApp.${parentEntityName}.name`}>Name</Translate>
              </span>
            </dt>
            <dd>{parentEntity.name}</dd>
          </dl>
          <Button tag={Link} to={`/${parentEntityName}`} replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/${parentEntityName}/${parentEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>

      {/* <Band match={match} /> */}

      {/* <Row>
        <Col md="12">
          <div className="table-responsive">
            {entitiesList && entitiesList.length > 0 ? (
              <Table responsive>
                <thead>
                  <tr>
                    <th>
                      <Translate contentKey="global.field.id">ID</Translate>
                    </th>
                    <th>
                      <Translate contentKey={`educationWebChatApp.${parentEntityName}.name`}>Name</Translate>
                    </th>
                    <th />
                  </tr>
                </thead>
                <tbody>
                  {entitiesList.map((entity, i) => (
                    <tr key={`entity-${i}`}>
                      <td>
                        <Button tag={Link} to={`${match.url}/${entitiesListName}/${entity.id}`} color="link" size="sm">
                          {entity.id}
                        </Button>
                      </td>
                      <td>{entity.name}</td>
                      <td className="text-right">
                        <div className="btn-group flex-btn-group-container">
                          <Button tag={Link} to={`${match.url}/${entity.id}`} color="info" size="sm">
                            <FontAwesomeIcon icon="eye" />{' '}
                            <span className="d-none d-md-inline">
                              <Translate contentKey="entity.action.view">View</Translate>
                            </span>
                          </Button>
                          <Button tag={Link} to={`${match.url}/${entity.id}/edit`} color="primary" size="sm">
                            <FontAwesomeIcon icon="pencil-alt" />{' '}
                            <span className="d-none d-md-inline">
                              <Translate contentKey="entity.action.edit">Edit</Translate>
                            </span>
                          </Button>
                          <Button tag={Link} to={`${match.url}/${entitiesListName}/${entity.id}/delete`} color="danger" size="sm">
                            <FontAwesomeIcon icon="trash" />{' '}
                            <span className="d-none d-md-inline">
                              <Translate contentKey="entity.action.delete">Delete</Translate>
                            </span>
                          </Button>
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            ) : (
              !loading && (
                <div className="alert alert-warning">
                  <Translate contentKey={`educationWebChatApp.${parentEntity.name}.home.notFound`}>No ${parentEntity.name} found</Translate>
                </div>
              )
            )}
          </div>
        </Col>
      </Row> */}
    </>
  )
}
