package ru.yuzum.webchat.service.dto;

import lombok.Data;
import lombok.ToString;
import ru.yuzum.webchat.domain.enumeration.DayOfWeek;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link ru.yuzum.webchat.domain.Lesson} entity.
 */
@Data
@ToString
public class LessonDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Instant time;

    private DayOfWeek dayOfWeek;

    private Long roomId;

    private String roomIdentifier;

    private Long bandId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LessonDTO lessonDTO = (LessonDTO) o;
        if (lessonDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lessonDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
