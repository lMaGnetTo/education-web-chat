package ru.yuzum.webchat.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ru.yuzum.webchat.domain.Institution} entity.
 */
public class InstitutionDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(min = 2)
    private String name;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InstitutionDTO institutionDTO = (InstitutionDTO) o;
        if (institutionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), institutionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InstitutionDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
