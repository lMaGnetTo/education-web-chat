package ru.yuzum.webchat.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import ru.yuzum.webchat.domain.enumeration.DayOfWeek;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link ru.yuzum.webchat.domain.Lesson} entity. This class is used
 * in {@link ru.yuzum.webchat.web.rest.LessonResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /lessons?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LessonCriteria implements Serializable, Criteria {
    /**
     * Class for filtering DayOfWeek
     */
    public static class DayOfWeekFilter extends Filter<DayOfWeek> {

        public DayOfWeekFilter() {
        }

        public DayOfWeekFilter(DayOfWeekFilter filter) {
            super(filter);
        }

        @Override
        public DayOfWeekFilter copy() {
            return new DayOfWeekFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private InstantFilter time;

    private DayOfWeekFilter dayOfWeek;

    private LongFilter roomId;

    private LongFilter bandId;

    public LessonCriteria() {
    }

    public LessonCriteria(LessonCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.time = other.time == null ? null : other.time.copy();
        this.dayOfWeek = other.dayOfWeek == null ? null : other.dayOfWeek.copy();
        this.roomId = other.roomId == null ? null : other.roomId.copy();
        this.bandId = other.bandId == null ? null : other.bandId.copy();
    }

    @Override
    public LessonCriteria copy() {
        return new LessonCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public InstantFilter getTime() {
        return time;
    }

    public void setTime(InstantFilter time) {
        this.time = time;
    }

    public DayOfWeekFilter getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeekFilter dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public LongFilter getRoomId() {
        return roomId;
    }

    public void setRoomId(LongFilter roomId) {
        this.roomId = roomId;
    }

    public LongFilter getBandId() {
        return bandId;
    }

    public void setBandId(LongFilter bandId) {
        this.bandId = bandId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LessonCriteria that = (LessonCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(time, that.time) &&
            Objects.equals(dayOfWeek, that.dayOfWeek) &&
            Objects.equals(roomId, that.roomId) &&
            Objects.equals(bandId, that.bandId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        time,
        dayOfWeek,
        roomId,
        bandId
        );
    }

    @Override
    public String toString() {
        return "LessonCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (time != null ? "time=" + time + ", " : "") +
                (dayOfWeek != null ? "dayOfWeek=" + dayOfWeek + ", " : "") +
                (roomId != null ? "roomId=" + roomId + ", " : "") +
                (bandId != null ? "bandId=" + bandId + ", " : "") +
            "}";
    }

}
