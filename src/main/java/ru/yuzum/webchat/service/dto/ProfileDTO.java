package ru.yuzum.webchat.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link ru.yuzum.webchat.domain.Profile} entity.
 */
public class ProfileDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(min = 2)
    private String fullName;


    private Long userId;
    private Set<BandDTO> bands = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<BandDTO> getBands() {
        return bands;
    }

    public void setBands(Set<BandDTO> bands) {
        this.bands = bands;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProfileDTO profileDTO = (ProfileDTO) o;
        if (profileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), profileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProfileDTO{" +
            "id=" + getId() +
            ", fullName='" + getFullName() + "'" +
            ", userId=" + getUserId() +
            ", bands='" + getBands() + "'" +
            "}";
    }
}
