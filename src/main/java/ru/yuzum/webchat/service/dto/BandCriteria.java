package ru.yuzum.webchat.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ru.yuzum.webchat.domain.Band} entity. This class is used
 * in {@link ru.yuzum.webchat.web.rest.BandResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /bands?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BandCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LongFilter lessonId;

    private LongFilter institutionId;

    private LongFilter profilesId;

    public BandCriteria() {
    }

    public BandCriteria(BandCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.lessonId = other.lessonId == null ? null : other.lessonId.copy();
        this.institutionId = other.institutionId == null ? null : other.institutionId.copy();
        this.profilesId = other.profilesId == null ? null : other.profilesId.copy();
    }

    @Override
    public BandCriteria copy() {
        return new BandCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getLessonId() {
        return lessonId;
    }

    public void setLessonId(LongFilter lessonId) {
        this.lessonId = lessonId;
    }

    public LongFilter getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(LongFilter institutionId) {
        this.institutionId = institutionId;
    }

    public LongFilter getProfilesId() {
        return profilesId;
    }

    public void setProfilesId(LongFilter profilesId) {
        this.profilesId = profilesId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BandCriteria that = (BandCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(lessonId, that.lessonId) &&
            Objects.equals(institutionId, that.institutionId) &&
            Objects.equals(profilesId, that.profilesId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        lessonId,
        institutionId,
        profilesId
        );
    }

    @Override
    public String toString() {
        return "BandCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (lessonId != null ? "lessonId=" + lessonId + ", " : "") +
                (institutionId != null ? "institutionId=" + institutionId + ", " : "") +
                (profilesId != null ? "profilesId=" + profilesId + ", " : "") +
            "}";
    }

}
