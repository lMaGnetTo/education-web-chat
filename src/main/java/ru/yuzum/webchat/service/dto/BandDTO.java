package ru.yuzum.webchat.service.dto;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ru.yuzum.webchat.domain.Band} entity.
 */
@Data
@ToString
public class BandDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private Long institutionId;

    private String institutionName;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BandDTO bandDTO = (BandDTO) o;
        if (bandDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bandDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
