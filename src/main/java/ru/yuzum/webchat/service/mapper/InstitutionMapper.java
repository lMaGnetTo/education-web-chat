package ru.yuzum.webchat.service.mapper;


import ru.yuzum.webchat.domain.*;
import ru.yuzum.webchat.service.dto.InstitutionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Institution} and its DTO {@link InstitutionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InstitutionMapper extends EntityMapper<InstitutionDTO, Institution> {


    @Mapping(target = "bands", ignore = true)
    @Mapping(target = "removeBand", ignore = true)
    Institution toEntity(InstitutionDTO institutionDTO);

    default Institution fromId(Long id) {
        if (id == null) {
            return null;
        }
        Institution institution = new Institution();
        institution.setId(id);
        return institution;
    }
}
