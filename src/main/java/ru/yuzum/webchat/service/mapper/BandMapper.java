package ru.yuzum.webchat.service.mapper;


import ru.yuzum.webchat.domain.*;
import ru.yuzum.webchat.service.dto.BandDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Band} and its DTO {@link BandDTO}.
 */
@Mapper(componentModel = "spring", uses = {InstitutionMapper.class})
public interface BandMapper extends EntityMapper<BandDTO, Band> {

    @Mapping(source = "institution.id", target = "institutionId")
    @Mapping(source = "institution.name", target = "institutionName")
    BandDTO toDto(Band band);

    @Mapping(target = "lessons", ignore = true)
    @Mapping(target = "removeLesson", ignore = true)
    @Mapping(source = "institutionId", target = "institution")
    @Mapping(target = "profiles", ignore = true)
    @Mapping(target = "removeProfiles", ignore = true)
    Band toEntity(BandDTO bandDTO);

    default Band fromId(Long id) {
        if (id == null) {
            return null;
        }
        Band band = new Band();
        band.setId(id);
        return band;
    }
}
