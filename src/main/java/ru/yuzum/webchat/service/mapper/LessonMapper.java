package ru.yuzum.webchat.service.mapper;


import ru.yuzum.webchat.domain.*;
import ru.yuzum.webchat.service.dto.LessonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Lesson} and its DTO {@link LessonDTO}.
 */
@Mapper(componentModel = "spring", uses = {RoomMapper.class, BandMapper.class})
public interface LessonMapper extends EntityMapper<LessonDTO, Lesson> {

    @Mapping(source = "room.id", target = "roomId")
    @Mapping(source = "band.id", target = "bandId")
    @Mapping(source = "room.identifier", target = "roomIdentifier")
    LessonDTO toDto(Lesson lesson);

    @Mapping(source = "roomId", target = "room")
    @Mapping(source = "bandId", target = "band")
    Lesson toEntity(LessonDTO lessonDTO);

    default Lesson fromId(Long id) {
        if (id == null) {
            return null;
        }
        Lesson lesson = new Lesson();
        lesson.setId(id);
        return lesson;
    }
}
