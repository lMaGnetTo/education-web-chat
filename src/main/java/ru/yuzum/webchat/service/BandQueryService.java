package ru.yuzum.webchat.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ru.yuzum.webchat.domain.Band;
import ru.yuzum.webchat.domain.*; // for static metamodels
import ru.yuzum.webchat.repository.BandRepository;
import ru.yuzum.webchat.service.dto.BandCriteria;
import ru.yuzum.webchat.service.dto.BandDTO;
import ru.yuzum.webchat.service.mapper.BandMapper;

/**
 * Service for executing complex queries for {@link Band} entities in the database.
 * The main input is a {@link BandCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BandDTO} or a {@link Page} of {@link BandDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BandQueryService extends QueryService<Band> {

    private final Logger log = LoggerFactory.getLogger(BandQueryService.class);

    private final BandRepository bandRepository;

    private final BandMapper bandMapper;

    public BandQueryService(BandRepository bandRepository, BandMapper bandMapper) {
        this.bandRepository = bandRepository;
        this.bandMapper = bandMapper;
    }

    /**
     * Return a {@link List} of {@link BandDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BandDTO> findByCriteria(BandCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Band> specification = createSpecification(criteria);
        return bandMapper.toDto(bandRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BandDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BandDTO> findByCriteria(BandCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Band> specification = createSpecification(criteria);
        return bandRepository.findAll(specification, page)
            .map(bandMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BandCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Band> specification = createSpecification(criteria);
        return bandRepository.count(specification);
    }

    /**
     * Function to convert {@link BandCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Band> createSpecification(BandCriteria criteria) {
        Specification<Band> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Band_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Band_.name));
            }
            if (criteria.getLessonId() != null) {
                specification = specification.and(buildSpecification(criteria.getLessonId(),
                    root -> root.join(Band_.lessons, JoinType.LEFT).get(Lesson_.id)));
            }
            if (criteria.getInstitutionId() != null) {
                specification = specification.and(buildSpecification(criteria.getInstitutionId(),
                    root -> root.join(Band_.institution, JoinType.LEFT).get(Institution_.id)));
            }
            if (criteria.getProfilesId() != null) {
                specification = specification.and(buildSpecification(criteria.getProfilesId(),
                    root -> root.join(Band_.profiles, JoinType.LEFT).get(Profile_.id)));
            }
        }
        return specification;
    }
}
