package ru.yuzum.webchat.service;

import ru.yuzum.webchat.domain.Band;
import ru.yuzum.webchat.repository.BandRepository;
import ru.yuzum.webchat.service.dto.BandDTO;
import ru.yuzum.webchat.service.mapper.BandMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Band}.
 */
@Service
@Transactional
public class BandService {

    private final Logger log = LoggerFactory.getLogger(BandService.class);

    private final BandRepository bandRepository;

    private final BandMapper bandMapper;

    public BandService(BandRepository bandRepository, BandMapper bandMapper) {
        this.bandRepository = bandRepository;
        this.bandMapper = bandMapper;
    }

    /**
     * Save a band.
     *
     * @param bandDTO the entity to save.
     * @return the persisted entity.
     */
    public BandDTO save(BandDTO bandDTO) {
        log.debug("Request to save Band : {}", bandDTO);
        Band band = bandMapper.toEntity(bandDTO);
        band = bandRepository.save(band);
        return bandMapper.toDto(band);
    }

    /**
     * Get all the bands.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<BandDTO> findAll() {
        log.debug("Request to get all Bands");
        return bandRepository.findAll().stream()
            .map(bandMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one band by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BandDTO> findOne(Long id) {
        log.debug("Request to get Band : {}", id);
        return bandRepository.findById(id)
            .map(bandMapper::toDto);
    }

    /**
     * Delete the band by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Band : {}", id);
        bandRepository.deleteById(id);
    }
}
