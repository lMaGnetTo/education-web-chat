package ru.yuzum.webchat.service;

import io.github.jhipster.security.RandomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yuzum.webchat.domain.Lesson;
import ru.yuzum.webchat.repository.LessonRepository;
import ru.yuzum.webchat.service.dto.LessonDTO;
import ru.yuzum.webchat.service.dto.RoomDTO;
import ru.yuzum.webchat.service.mapper.LessonMapper;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Lesson}.
 */
@Service
@Transactional
public class LessonService {

    private final Logger log = LoggerFactory.getLogger(LessonService.class);

    private final LessonRepository lessonRepository;

    private final LessonMapper lessonMapper;

    private final RoomService roomService;

    public LessonService(LessonRepository lessonRepository, LessonMapper lessonMapper, RoomService roomService) {
        this.lessonRepository = lessonRepository;
        this.lessonMapper = lessonMapper;
        this.roomService = roomService;
    }

    /**
     * Save a lesson.
     *
     * @param lessonDTO the entity to save.
     * @return the persisted entity.
     */
    public LessonDTO save(LessonDTO lessonDTO) {
        log.debug("Request to save Lesson : {}", lessonDTO);
        if (lessonDTO.getId() == null) {
            RoomDTO lessonRoom = createLessonRoom();
            lessonDTO.setRoomId(lessonRoom.getId());
        }
        Lesson lesson = lessonMapper.toEntity(lessonDTO);
        lesson = lessonRepository.save(lesson);
        return lessonMapper.toDto(lesson);
    }

    /**
     * Create a lesson room.
     *
     * @return the persisted entity.
     */
    private RoomDTO createLessonRoom() {
        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setIdentifier(RandomUtil.generateRandomAlphanumericString());
        return roomService.save(roomDTO);
    }

    /**
     * Get all the lessons.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<LessonDTO> findAll() {
        log.debug("Request to get all Lessons");
        return lessonRepository.findAll().stream()
            .map(lessonMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one lesson by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<LessonDTO> findOne(Long id) {
        log.debug("Request to get Lesson : {}", id);
        return lessonRepository.findById(id)
            .map(lessonMapper::toDto);
    }

    /**
     * Delete the lesson by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Lesson : {}", id);
        lessonRepository.deleteById(id);
    }
}
