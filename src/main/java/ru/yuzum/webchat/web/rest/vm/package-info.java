/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.yuzum.webchat.web.rest.vm;
