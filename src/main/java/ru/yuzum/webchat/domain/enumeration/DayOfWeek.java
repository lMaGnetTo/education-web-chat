package ru.yuzum.webchat.domain.enumeration;

/**
 * The DayOfWeek enumeration.
 */
public enum DayOfWeek {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}
